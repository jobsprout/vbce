class CreateCurrencies < ActiveRecord::Migration
  def change
    create_table :currencies do |t|
      t.string :iso
      t.string :country
      t.decimal :buy_price
      t.decimal :sell_price

      t.timestamps null: false
    end
  end
end
