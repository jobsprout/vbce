class Admin::RatesController < ApplicationController
  def index
  	@rates = Currency.all
  end

  def new
  	@rate = Currency.new
  end

  def create
  	@rate = Currency.new(currency_params)
  	if @rate.save
  		redirect_to admin_rates_path, notice: "#{@rate.country} successfully created."
  	else
  		render :new
  	end
  end

  def edit
  	@rate = Currency.find(params[:id])
  end

  def update
  	@rate = Currency.find(params[:id])
  	if @rate.update(currency_params)
  		redirect_to admin_rates_path, notice: "#{@rate.country} successfully updated."
  	else
  		render :edit
  	end
  end

  def destroy
  	@rate = Currency.find(params[:id]).destroy
  	redirect_to admin_rates_path, notice: "#{@rate.country} successfully removed."
  end

  private

  def currency_params
  	params.require(:currency).permit!
  end
end
