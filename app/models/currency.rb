class Currency < ActiveRecord::Base
	validates :iso, :country, :buy_price, :sell_price, presence: true
	validates :iso, :country, uniqueness: true
end
