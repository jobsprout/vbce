class RatesController < ApplicationController
  def index
  	if params[:search]
  		@rates = Currency.where("iso ilike ?", "%#{params[:search]}%")
  	else
  		@rates = Currency.order("ISO asc")
  	end
  end
end
